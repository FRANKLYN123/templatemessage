class Main{
  public static void main(String[] args){
    Port p = new Port();
    Transmitter t = new Transmitter(p);
    Receiver r = new Receiver(p);

    t.start();
    r.start();

  }
}
