class Transmitter extends Thread{
  private Port p;
  public Transmitter(Port p){ this.p = p; }
  public void run(){
    Message m = new Message();
    System.out.println("Enviando el mensaje");
    p.send(m);
    System.out.println("Mensaje enviado, sigo con mi vida!");
  }
}
