class Receiver extends Thread{
  private Port p;
  public Receiver(Port p){ this.p = p; }
  public void run(){
    System.out.println("Esperando recibir un mensaje");
    Message m = p.receive();
    System.out.println("Mensaje recibido");
    p.reply(m);
  }
}
